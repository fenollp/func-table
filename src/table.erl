%% Copyright © 2014 Pierre Fenoll ‹pierrefenoll@gmail.com›
%% See LICENSE for licensing information.
%% -*- coding: utf-8 -*-
-module(table).

%% table: store atoms and that's an hash table!

-export([ new/0
        , add/3
      %%, remove/2
        , fetch/3 ]).

-define(zilch, empty_table).

%% API

new () ->
    ?zilch.

add (Key, Value, ?zilch) ->
    fun (K) ->
            case K of
                Key -> Value
            end
    end;
add (Key, Value, Table) ->
    fun (K) ->
            case K of
                Key -> Value;
                _K  -> Table(_K)
            end
    end.

fetch (_, Default, ?zilch) ->
    Default;
fetch (Key, Default, Table) ->
    try Table(Key) of
        Value -> Value
    catch
        error:function_clause -> Default;
        error:{case_clause,Key} -> Default
    end.

%% End of Module.
